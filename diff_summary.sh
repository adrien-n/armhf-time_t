#!/bin/bash

. ./lib.sh

set -e
set -u
shopt -s inherit_errexit

sql_join_on_select() {
  local join_on
  local where_clause

  join_on="$1"
  where_clause="$2"

  sqlite3 "${SQLITE_DB}" \
    "select devpkg from packages inner join ${join_on} on packages.rowid = ${join_on}.id where ${where_clause} order by packages.devpkg asc;"
}

sql_analysis_select() {
  sql_join_on_select 'analysis' "$@"
}

sql_dumps_select() {
  sql_join_on_select 'dumps' "$@"
}

mkdir -p 'out/summary'

all_in_one='out/summary/results_all_in_one.txt'

: > 'out/summary/results_all_in_one.txt'

sql_analysis_select 'analysis.time_t' \
  | tee 'out/summary/results_time_t.txt' \
  | sed 's/^/time_t: /' \
  >> "${all_in_one}"

sql_analysis_select 'analysis.lfs' \
  | tee 'out/summary/results_lfs.txt' \
  | sed 's/^/lfs: /' \
  >> "${all_in_one}"

sql_analysis_select 'analysis.lfs and analysis.time_t' \
  | tee 'out/summary/results_lfs_and_time_t.txt' \
  | sed 's/^/lfs-and-time_t: /' \
  >> "${all_in_one}"

sql_analysis_select 'analysis.lfs and not analysis.time_t' \
  | tee 'out/summary/results_lfs_not_time_t.txt' \
  | sed 's/^/lfs-not-time_t: /' \
  >> "${all_in_one}"

sql_analysis_select 'not analysis.lfs and analysis.time_t' \
  | tee 'out/summary/results_not_lfs_and_time_t.txt' \
  | sed 's/^/not-lfs-and-time_t: /' \
  >> "${all_in_one}"

sql_analysis_select 'analysis.lfs or analysis.time_t' \
  | tee 'out/summary/results_lfs_or_time_t.txt' \
  | sed 's/^/lfs-or-time_t: /' \
  >> "${all_in_one}"

sql_analysis_select \
  'analysis.analyzed and not analysis.lfs and not analysis.time_t' \
  | tee 'out/summary/results_none.txt' \
  | sed 's/^/none: /' \
  >> "${all_in_one}"

for status in 'failed' 'uninstallable' 'skipped' 'dumped'; do
  sql_dumps_select "dumps.status in ('${status}')" \
    | tee "out/summary/results_${status}.txt" \
    | sed "s/^/status-${status}: /" \
    >> "${all_in_one}"
done

