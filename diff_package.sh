#!/bin/bash

. ./lib.sh

set -e
set -u
shopt -s inherit_errexit

clean_up() {
    local trap

    local from

    trap="$1"

    if [[ "${trap}" != 'EXIT' ]]; then
        if [[ ${BASH_LINENO[0]} -eq 1 ]]; then
            from="${trap} trap"
            trap - INT TERM ERR EXIT
        else
            from="line ${BASH_LINENO[0]}"
        fi
        log_msg "Clean up (called from ${from})"
    fi
}

# Run a-c-c's diff
a_c_c_diff() {
    local devpkg
    local old
    local new

    devpkg="$1"
    old="$2"
    new="$3"

    if ! abi-compliance-checker -s -l "$devpkg" \
        -filter "out/diffs/${devpkg}/${devpkg}_base.xml" \
        -old "out/diffs/${devpkg}/${devpkg}_${old}.dump" \
        -new "out/diffs/${devpkg}/${devpkg}_${new}.dump" \
        -report-path "out/compat_reports/${devpkg}/${old}_to_${new}/compat_report.html" \
        -bin-report-path "out/compat_reports/${devpkg}/${old}_to_${new}/abi_compat_report.html" \
        -src-report-path "out/compat_reports/${devpkg}/${old}_to_${new}/src_compat_report.html" \
        > /dev/null
    then
        test -z "$(grep -E "class='(failed|chg)'" \
            "out/compat_reports/${devpkg}/${old}_to_${new}/compat_report.html" \
        | grep -v 'Removed Symbols')"
    fi
}

diff() {
    local devpkg

    local lfs='false'
    local time_t='false'

    devpkg="$1"

    # Extract to the diffs directory
    mkdir -p 'out/diffs'
    tar xf "out/dumps/${devpkg}.dump.tar.xz" -C 'out/diffs'

    log_msg "=> ${devpkg}: base=>lfs, lfs=>time_t"

    a_c_c_diff "$devpkg" 'lfs' 'time_t' || time_t='true'
    a_c_c_diff "$devpkg" 'base' 'lfs' || lfs='true'

    analysis_sql_result "${devpkg}" "${lfs}" "${time_t}"
}

trap 'clean_up INT' INT
trap 'clean_up TERM' TERM
trap 'clean_up ERR' ERR
trap 'clean_up EXIT' EXIT

skip_diffed='false'

case "$1" in
  --skip-diffed)
    skip_diffed='true'
    shift
    ;;
esac

diffed="$(sqlite3 "${SQLITE_DB}" "select 'true' from packages inner join analysis on packages.rowid = analysis.id where analysis.analyzed and devpkg = 'zlib1g-dev';")"

if ! { ${skip_diffed} && ${diffed} ; }; then
    diff "$1"
fi
