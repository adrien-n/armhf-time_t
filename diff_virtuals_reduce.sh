#!/bin/bash

. ./lib.sh

set -e
set -u
shopt -s inherit_errexit

packages_with_virtuals() {
    sqlite3 "${SQLITE_DB}" 'select real from virtual_packages group by real order by real asc;'
}

packages_virtuals_not_dumped_names() {
    local devpkg_no_virtual

    devpkg_no_virtual="$1"

    sqlite3 "${SQLITE_DB}" "select devpkg from packages where rowid in
(select dumps.id from dumps inner join virtual_packages on dumps.id = virtual_packages.id where dumps.status is not 'dumped' and virtual_packages.real='${devpkg_no_virtual}' order by dumps.id asc);"
}

packages_virtuals_any_lfs_or_time_t() {
    local devpkg_no_virtual
    local lfs_or_time

    devpkg_no_virtual="$1"
    lfs_or_time="$2"

    sqlite3 "${SQLITE_DB}" "select analysis.id from analysis where analysis.${lfs_or_time} and id in (select dumps.id from dumps inner join virtual_packages on dumps.id = virtual_packages.id where dumps.status is 'dumped' and virtual_packages.real='${devpkg_no_virtual}' order by dumps.id asc);"
}

packages_virtuals_reduce() {
    local devpkg_no_virtual

    local lfs
    local time_t
    local not_dumped

    devpkg_no_virtual="$1"

    lfs='false'
    time_t='false'

    not_dumped="$(packages_virtuals_not_dumped_names "${devpkg_no_virtual}")"

    if [[ -n "${not_dumped}" ]]; then
        log_msg "${devpkg_no_virtual} has non-dumped splits: $(echo "${not_dumped}" | tr '\n' ' ')"
        return 0
    fi

    if [[ -n "$(packages_virtuals_any_lfs_or_time_t "${devpkg_no_virtual}" 'lfs')" ]]; then
        lfs="true"
    fi

    if [[ -n "$(packages_virtuals_any_lfs_or_time_t "${devpkg_no_virtual}" 'time_t')" ]]; then
        time_t="true"
    fi

    analysis_sql_result "${devpkg_no_virtual}" "${lfs}" "${time_t}"
}

for devpkg_no_virtual in $(packages_with_virtuals); do
    packages_virtuals_reduce "${devpkg_no_virtual}"
done
