#!/bin/sh

podman \
  run \
  --root=/media/sda1/adrien/containers-storage/ \
  --rm \
  -it \
  -v $PWD:$PWD \
  localhost/unstable-armhf:latest \
  env -C $PWD \
  http_proxy=http://temp-mantic:8000 \
  https_proxy=http://temp-mantic:8000 \
  "$@"
